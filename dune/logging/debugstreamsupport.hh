// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOGGING_DEBUGSTREAMSUPPORT_HH
#define DUNE_LOGGING_DEBUGSTREAMSUPPORT_HH

#include <functional>
#include <memory>
#include <unordered_map>

#include <dune/common/debugstream.hh>
#include <dune/common/parametertree.hh>

#include <dune/logging/loggingstream.hh>

namespace Dune::Logging {

#ifndef DOXYGEN

  // The contents of this file is only for internal purposes

  // This class maintains a database of known DebugStream instances and their default options as
  // well as their current attachment state to the logging system. Streams are attached by creating
  // a logging stream, which is also stored in the associated StreamState object, and attaching the
  // debug stream to this LoggingStream. The attaching and detaching are handled through type-erased
  // std::function objects because DebugStream is a template.
  class DebugStreamState
  {

    // state object for a single stream
    struct StreamState
    {
      LogLevel default_level;
      std::string default_backend;
      std::shared_ptr<LoggingStream> logging_stream;
      std::function<void(LoggingStream&)> attach;
      std::function<void()> detach;


      StreamState(
        LogLevel default_level,
        std::string_view default_backend,
        std::shared_ptr<LoggingStream> logging_stream,
        std::function<void(LoggingStream&)> attach,
        std::function<void()> detach
        );

      StreamState() = default;

      ~StreamState();

    };

    // Map of name-indexed debug stream state objects
    std::unordered_map<std::string,StreamState> _streams;

  public:

    // captures the debug stream with given name and applies the config options
    void capture(const std::string& name, const ParameterTree& config);

    // registers a debug stream with the database. You need to specify a name under
    // which the stream will be known, the actual stream, whether the stream should
    // be detached upon shutdown (always set to false for stack-allocated debug streams!),
    // the default level at which this stream will log if the config options during capture
    // do not specify something else, and the default backend to which the stream will log.
    template<typename Stream>
    void registerStream(
      const std::string& name,
      Stream& stream,
      bool detach,
      LogLevel default_level,
      std::string_view default_backend
      )
    {
      assert(_streams.count(name) == 0);
      _streams[name] = StreamState{
        default_level,
        default_backend,
        nullptr,
        [&](LoggingStream& logging_stream) {
          stream.attach(logging_stream);
        },
        [&]() {
          stream.detach();
        }
      };
      // If the stream should not be detached, just reset the corresponding function
      if (not detach)
        _streams[name].detach = nullptr;
    }

    DebugStreamState();
    ~DebugStreamState();

    // Captures all registered streams, reading options from the given ParameterTree.
    void captureStreams(const ParameterTree& config);

  };

#endif // DOXYGEN

} // namespace Dune::Logging

#endif // DUNE_LOGGING_DEBUGSTREAMSUPPORT_HH
